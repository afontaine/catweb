FROM golang:1.19

WORKDIR /usr/src/app

# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
COPY go.mod go.sum ./
RUN go mod download && go mod verify

COPY . .
RUN go build -v -o /usr/local/bin/app ./...
COPY templates/index.html .
COPY static/* /static/
EXPOSE 5000
CMD ["app"]

# Using the next for a demo issue to optimize the container
#FROM golang:1.20 AS builder
#
#WORKDIR /usr/src/app
#
# pre-copy/cache go.mod for pre-downloading dependencies and only redownloading them in subsequent builds if they change
#COPY . .
#RUN go mod download && go mod verify
#
# DO NOT UNCOMMENT RUN go build -v -o /usr/local/bin/app ./...
#RUN go build -v -ldflags "-linkmode 'external' -extldflags '-static'" -o /usr/local/bin/app ./... # https://mt165.co.uk/blog/static-link-go/
#FROM alpine:latest
#RUN mkdir -p /usr/src/app
#RUN mkdir -p /usr/src/app/static
#RUN mkdir -p /usr/src/app/templates
#
#WORKDIR /usr/src/app
#COPY --from=builder /usr/local/bin/app /usr/local/bin
#COPY --from=builder /usr/src/app/templates/index.html .
#COPY --from=builder /usr/src/app/static/* /usr/src/app/static
#EXPOSE 5000
#CMD ["app"]

